from .main import cli
from . import elt, schema, discovery, initialize, add, install, invoke, www, permissions


def main():
    cli()
